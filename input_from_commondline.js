// console.log(process.argv);
// console.log(process.argv[3]);
// run  in terminal like  this
//node input_from_commondline hello Naushad

//output
// [
//     'C:\\Program Files\\nodejs\\node.exe',
//     'C:\\Users\\naushad husain\\Desktop\\learning_node_js\\input_from_commondline',
//     'hello',
//     'Naushad'
//   ]


// use to create file in folder using procees object using commond line argumnent
const fs=require('fs');


const input =process.argv
if(input[2]=='add'){
    fs.writeFileSync(input[3],input[4])
}else if(input[2]=='remove'){
    fs.unlinkSync(input[3])
}

// add=> the file based on commond condition
// node input_from_commondline.js add apple.txt 'this is apple fruit'
// remove=>the file based on commond condition
// node input_from_commondline.js remove apple.txt
